package it.unimi.di.vec.ass1;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class GabmusTriangle implements Triangle {

  private int side0;
  private int side1;
  private int side2;

  private void validate() {
    if (side0 <= 0 || side1 <= 0 || side2 <= 0)
      throw new IllegalArgumentException("Side values cannot be negative or zero");
    if (side0 >= side1 + side2 || side1 >= side0 + side2 || side2 >= side0 + side1)
      throw new IllegalArgumentException("One side cannot be bigger than the sum of the other two");
  }

  public GabmusTriangle() throws IllegalArgumentException {
    this(System.in);
  }

  GabmusTriangle(InputStream in) throws IllegalArgumentException {
    // read from stdin
    Scanner scanner_in = new Scanner(in);
    side0 = Integer.parseInt(scanner_in.next());
    side1 = Integer.parseInt(scanner_in.next());
    side2 = Integer.parseInt(scanner_in.next());
    scanner_in.close();
    validate();
  }

  @Override
  public void describe() {
    describe(System.out);
  }

  void describe(PrintStream out) {
    if (side0 == side1 && side1 == side2) out.println("equilateral");
    else if (side0 == side1 || side1 == side2 || side0 == side2) out.println("isosceles");
    else out.println("scalene");
  }
}
