package it.unimi.di.vec.ass1;

public interface Triangle {

  // the constructor reads 3 integers from standard input interpreting them
  // as the lengths of the sides of the triangle

  // write in the standard output the type of triangle:
  // equilateral, isosceles, scalene
  void describe();
}
