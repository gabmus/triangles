package it.unimi.di.vec.ass1;

import static org.assertj.core.api.Assertions.*;

import java.io.*;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@Tag("StandardTest")
public class TestDescribe implements GabmusTriangleTest {

  @ParameterizedTest
  @CsvSource({
    "'3\n3\n3\n', 'equilateral'",
    "'4\n4\n1\n', 'isosceles'",
    "'4\n1\n4\n', 'isosceles'",
    "'1\n4\n4\n', 'isosceles'",
    "'3\n4\n5\n', 'scalene'"
  })
  public void testDescribeTriangle(String input, String expected) throws Exception {
    GabmusTriangle t = new GabmusTriangle(getInputStream(input));
    OutputStream out = getOutputStream();
    t.describe(new PrintStream(out));
    assertThat(out.toString().trim()).isEqualTo(expected);
  }
}
