package it.unimi.di.vec.ass1;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@Tag("ExceptionTest")
public class TestIllegalArguments implements GabmusTriangleTest {

  @ParameterizedTest
  @CsvSource({
    "'0\n3\n3\n',       'zero first',       'Side values cannot be negative or zero'",
    "'3\n0\n3\n',       'zero second',      'Side values cannot be negative or zero'",
    "'3\n3\n0\n',       'zero third',       'Side values cannot be negative or zero'",
    "'0\n0\n0\n',       'all zeros',        'Side values cannot be negative or zero'",
    "'foo\n3\n3\n',     'string first',     'For input string: \"'",
    "'3\nfoo\n3\n',     'string second',    'For input string: \"'",
    "'foo\nbar\nbaz\n', 'string all',       'For input string: \"'",
    "'3.2\n3\n3\n',     'float first',      'For input string: \"'",
    "'3\n3.2\n3\n',     'float second',     'For input string: \"'",
    "'3.3\n3.9\n3.5\n', 'all float',        'For input string: \"'",
    "'100\n3\n3\n',     'huge side first',  'One side cannot be bigger than the sum of the other two'",
    "'3\n100\n3\n',     'huge side second', 'One side cannot be bigger than the sum of the other two'",
    "'3\n3\n100\n',     'huge side third',  'One side cannot be bigger than the sum of the other two'",
    "'-3\n3\n3\n',      'negative first',   'Side values cannot be negative or zero'",
    "'3\n-3\n3\n',      'negative second',  'Side values cannot be negative or zero'",
    "'3\n3\n-3\n',      'negative third',   'Side values cannot be negative or zero'",
    "'-3\n-4\n-5\n',    'all negative',     'Side values cannot be negative or zero'"
  })
  public void testIllegalArgumentExceptionOnCreateWrongTriangle(
      String input, String testCase, String exceptionMessage) throws Exception {
    assertThatThrownBy(
            () -> {
              GabmusTriangle t = new GabmusTriangle(getInputStream(input));
            })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining(exceptionMessage);
  }
}
