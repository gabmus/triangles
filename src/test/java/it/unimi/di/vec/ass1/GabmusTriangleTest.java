package it.unimi.di.vec.ass1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public interface GabmusTriangleTest {

  default InputStream getInputStream(String s) {
    return new ByteArrayInputStream(s.getBytes());
  }

  default OutputStream getOutputStream() {
    return new ByteArrayOutputStream();
  }
}
